# SparkTech Redux

## CONTENTS
- Durasteel rail platform. For those who don't like riding on lights.
- New teleportation beacon, crafted from ore. Easier to build in the late game, but has only one outpost-styled model for all races.
- Compactor crafting station, available at Engineer's Table (Inventor's Table lvl 2). Compactor transforms loose material blocks into more dense forms, but processes only solid materials. Available recipes:
* 2 loose silt > 1 sandstone
* 2 sand > 1 sandstone
* 2 gravel > 1 cobblestone
* 2 stone rubble > 1 stone brick
* 2 stone rubble > 1 small stone brick
* 2 ash > 1 obsidian

## TODO
- Grinder to reverse the compacting process.
- Alternate models for mod's crafting stations.
- Rebalance compactor recipe to normal requirements.
- Complete compactor art.
- Weather detector
- slush to snow
- snow to ice
- obsidian to shadow rock melt

##CONFLICTS

No known conflicts at the moment.

## THANKS
C0bra5 - for his useful Recolor Tool (http://community.playstarbound.com/threads/update-v1-1-2-recolor-maker-2.105981/)

It would take me ages to create palettes without it.

C0rdyc3p5 - for his friendly SMK, the StarModder's Kit (http://community.playstarbound.com/threads/starmodder-kit-the-modding-ide-wip.117686/)

I usually use it for packing/unpacking my .pak files.
